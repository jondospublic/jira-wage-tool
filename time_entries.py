#!/usr/bin/python3
from jira import JIRA
import sys 
import click
import time

from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta

from settings import JIRA_AUTH_USERNAME, JIRA_PASSWORD, JIRA_SERVER, JIRA_USERNAMES, JIRA_PROJECT



# feel free to suggest more sensible defaults
today = date.today()
last_month = today - relativedelta(months=1)
from_date_default = date(last_month.year, last_month.month, 1)
to_date_default = date(today.year, today.month, 1) - relativedelta(days=1)
 
from_date_input = input(f"From ({from_date_default}): ")
from_date = datetime.strptime(from_date_input, "%Y-%m-%d").date() if from_date_input else from_date_default
to_date_input = input(f"To ({to_date_default}): ")
to_date = datetime.strptime(to_date_input, "%Y-%m-%d").date() if to_date_input else to_date_default


	
if not JIRA_USERNAMES:
	print(f"Please provide a JIRA_USERNAMES dictionary: user1:wage1, user2:wage2, ...")
	exit(1)
	
jira = JIRA(JIRA_SERVER, basic_auth=(JIRA_AUTH_USERNAME, JIRA_PASSWORD))
jira_query = ("worklogAuthor IN (" + '"{0}"'.format('", "'.join(JIRA_USERNAMES.keys())) + ")" if JIRA_USERNAMES else "") + (( " AND " if JIRA_USERNAMES else "") + "project=" + JIRA_PROJECT if JIRA_PROJECT else "")

print(f"JIRA query: {jira_query}")
sys.stdout.flush()
issues = jira.search_issues(jira_query, maxResults=False)

jira_time_spent = 0
jira_issue_count = 0
jira_costs = 0

jira_worklogs = {}

print("Parsing worklogs of {} issues, please wait...".format(len(issues)), flush=True)

        
with click.progressbar(issues, length=len(issues), fill_char='=', empty_char=' ') as bar:
	for issue in bar:
		issue_time_spent = 0
		issue_costs = 0
		issue_worklog_count = 0

		#print(".", end='', flush=True) # progress dot
		#print(issue.raw, flush=True)
		#print(issue.id + ":", end="")
		#sys.stdout.flush()
		
		worklogs = jira.worklogs(issue)
		for worklog in worklogs:
			worklog_date = datetime.strptime(worklog.started, "%Y-%m-%dT%H:%M:%S.%f%z").date()
			worklog_date_created = datetime.strptime(worklog.started, "%Y-%m-%dT%H:%M:%S.%f%z").date()
			#print(worklog.raw, flush=True)
			#print(worklog.author.raw, flush=True)
			#print("\n---\n", end='', flush=True)
			
			username = None
			if (hasattr(worklog.author, 'emailAddress') and worklog.author.emailAddress in JIRA_USERNAMES.keys()):
				username = worklog.author.emailAddress
			elif hasattr(worklog.author, 'accountId') and worklog.author.accountId in JIRA_USERNAMES.keys():
				username = worklog.author.accountId
			
			if (username is not None and (worklog_date >= from_date and worklog_date <= to_date)):
				
				jira_worklogs[issue.key + ":" + str(JIRA_USERNAMES[username])] = jira_worklogs.get(issue.key + ":" + str(JIRA_USERNAMES[username]), []) + [worklog]
				issue_time_spent += worklog.timeSpentSeconds / 3600
				issue_worklog_count += 1
				issue_costs +=  (worklog.timeSpentSeconds / 3600) * JIRA_USERNAMES[username]
				#print(f" {worklog.id} ({worklog.timeSpentSeconds / 3600:.2f} h)", end="")
				
				if worklog_date_created != worklog_date:
					print("Worklog {} for {} has a creation date ({}) different from worklog date ({})!".format(worklog.id, issue.key, worklog_date_created, worklog_date), flush=True)

		jira_time_spent += issue_time_spent
		jira_issue_count += 1
		jira_costs += issue_costs

		#if worklog_date >= from_date and worklog_date <= to_date:
			#print(f"; {issue_time_spent:.2f} h total, {issue_time_spent / issue_worklog_count if issue_worklog_count != 0 else 0:.2f} h avg")

print("\n")
		
for issue, worklogs in jira_worklogs.items():

	#print(f"{issue}: {[(worklog.id, f'{worklog.timeSpentSeconds / 3600:.2f} h') for worklog in worklogs]}; {sum(worklog.timeSpentSeconds / 3600 for worklog in worklogs)} h total, {sum(worklog.timeSpentSeconds / 3600 for worklog in worklogs) / len(worklogs) if len(worklogs) != 0 else 0:.2f} h avg")
	print(f"{issue}: {sum(worklog.timeSpentSeconds / 3600 for worklog in worklogs):.2f} h total; {[(worklog.id, f'{worklog.timeSpentSeconds / 3600:.2f} h') for worklog in worklogs]}")

print("\n")
#print(f"JIRA: {jira_time_spent:.2f} h total, {jira_time_spent / jira_issue_count if jira_issue_count != 0 else 0:.2f} h avg, {jira_costs:.2f} EUR")
print(f"JIRA: {jira_time_spent:.2f} h total")
print("\n\n\n")
sys.stdout.flush()

print(f"total: {jira_time_spent:.2f} h, {jira_costs:.2f} EUR")
