To get this working, you only need to follow ~~3~~ 4 simple steps:

1. create `settings.py` to set the auth variables (`JIRA_AUTH_USERNAME`, `JIRA_PASSWORD`) and developer ID/username (`JIRA_USERNAME`).
2. Insert personal Atlassian Server password, or for cloud:  Create API token instead of "password": https://id.atlassian.com/manage/api-tokens
3. `pipenv install`
4. `pipenv shell`
5. `python3 time_entries.py`

